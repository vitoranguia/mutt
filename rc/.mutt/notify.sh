#!/usr/bin/env sh
#
# Mail notify
#

if [ ! -d "$1" ]; then
    notify-send "Mail" "$1 it is not a directory" --icon=dialog-warning
    exit
fi

count="$(find "$1" -type f -name '*:2,' -printf '.' | wc -c)"

if [ "$count" -ge 1 ]; then
    notify-send "Mail" "New email at $1" --icon=mail-unread
fi
