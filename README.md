# Mutt 

My advance configuration [Mutt](http://www.mutt.org) with `CardDav`
and desktop notification

Don't ask me about Gmail, Outlook and the like

![Print screen](https://pixelfed.social/storage/m/113a3e2124a33b1f5511e531953f5ee48456e0c7/85c26953a179a0e533565ce79e82deb9724813d5/Vpfou16RLMi20J2pxCCFEMg4csDxQ2OVNzW5uKGl.png)

Requirements

- [Desktop notification](https://wiki.archlinux.org/index.php/Desktop_notifications)
- Email account
- [NextCloud Contacts](https://apps.nextcloud.com/apps/contacts)

Install

```
# apt install -y vim-nox lynx urlview offlineimap mutt vdirsyncer khard notify-send
```

```
$ cp -r rc/* "$HOME"/
```


## Configure

In the examples below uses multiple accounts (personal and work)
remove, replicate and rename, if necessary

### Email (IMAP)

Change the uppercase words in the `"$HOME"/.offlineimaprc` file,
according to your Email provider

```
remotehost = PROVIDER
remoteuser = USER
remotepass = PASSWORD
```

Note: Cache location `"$HOME"/.offlineimap`

### Email (SMTP)

Change the uppercase words in the `"$HOME"/.mutt/muttrc_work` file,
according to your Email provider

```
set smtp_url = 'smtp://EMAIL:PASSWORD@PROVIDER:587'
set from = 'EMAIL'
set realname = 'NAME'
```

### NextCloud Contacts (CardDav)

Change the uppercase words in the `"$HOME"/.config/vdirsyncer` file,
according to your NextCloud provider

```
url = "PROVIDER"
username = "USER"
password = "PASSWORD"
```

Changes for contacts to be synchronized

```
collections = ["personal", "work"]
```

Create local contacts

```
$ vdirsyncer discover contacts
```

Note: Cache location `"$HOME"/.vdirsyncer`

Change the uppercase words in the `"$HOME"/.config/khard/khard.conf` file,
according to your NextCloud provider

```
[addressbooks]
[[personal]]
path = ~/.contacts/PERSONAL
[[work]]
path = ~/.contacts/WORK
```

## Basic use

Contact sync

```
$ vdirsyncer sync
```

Mail sync

```
$ offlineimap -a personal 
```

Mail open

```
$ mutt -F ~/.mail/muttrc_personal
```

### Custom

<kbd>a</kbd> add contact 

<kbd>Tab</kbd> autocomplete contact 

<kbd>Ctrl</kbd> + <kbd>b</kbd> list URLs

### Optional 

Auto sync, execute `$ crontab -e` and add the lines

```
DISPLAY=:0
*/15 * * * * /bin/offlineimap -u quiet -a personal && "$HOME"/.mutt/notify.sh "$HOME"/.mail/personal/
*/15 * * * * /bin/offlineimap -u quiet -a work && "$HOME"/.mutt/notify.sh "$HOME"/.mail/work/
*/15 * * * * /bin/vdirsyncer -v WARNING sync
```

Alias, add the lines in the `"$HOME"/.bashrc` file

```
alias mail_personal="mutt -F ~/.mutt/muttrc_personal"
alias mail_work="mutt -F ~/.mutt/muttrc_work"
```

